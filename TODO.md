### Nice to have:
- [ ] Mock logging during the test running
- [ ] Use channel and goroutines (for rss feed and users)
- [ ] Clear logs, make them meaningful
- [ ] Write environment to logs, [example](https://huynvk.dev/blog/4-tips-for-logging-on-gcp-using-golang-and-logrus)

### Extra:
- [ ] Add tests:
    - [ ] Complex cases with mocks (maybe some parts of app are not testable, so need to make them testable)

### Done:
- [x] Create a template for an email with the list of new manga chapters (might be improved)
- [x] Check token expiration time and update if needed
- [x] Create a command to check new chapters
- [x] Add a new job to run the command once a week
- [x] Add a README.md
- [x] Logging ([Papertrail](https://papertrailapp.com/), for example)
- [x] Add tests:
    - [x] Simple test case
    - [x] Create a job to execute tests in CI
- [x] Move recipient's data to DB
- [x] Prepare for multi-user usage
- [x] Solve request rate limit for Shikimori
- [x] Introduce interfaces and use them for DI
