package pkg

import (
	"bytes"
	log "github.com/sirupsen/logrus"
	"html/template"
	"net/smtp"
	"time"
)

const (
	MIME       = "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	TimeFormat = "02 Jan 2006"
)

type SMTPClientInterface interface {
	Send(to string, username string, links []NewChapters)
}

type SMTPClient struct {
	Host        string
	Port        string
	Credentials SMTPCredentials
}

type SMTPCredentials struct {
	Email    string
	Password string
}

type Email struct {
	from    string
	to      string
	subject string
	body    string
}

type Items struct {
	Username string
	Chapters []NewChapters
}

func (client SMTPClient) Send(to string, username string, links []NewChapters) {
	html, err := parseTemplate("templates/email.html", Items{Username: username, Chapters: links})
	if err != nil {
		log.WithError(err).Fatal("Couldn't parse html template")
		return
	}

	subject := getSubject()
	msg := "To: " + to + "\r\nSubject: " + subject + "\r\n" + MIME + "\r\n" + html
	auth := smtp.PlainAuth("", client.Credentials.Email, client.Credentials.Password, client.Host)

	err = smtp.SendMail(client.getAddress(), auth, client.Credentials.Email, []string{to}, []byte(msg))
	if err != nil {
		log.WithError(err).Fatal("Couldn't send smtp message")
		return
	}

	log.Info("Email with new chapters was sent")
}

func (client SMTPClient) getAddress() string {
	return client.Host + ":" + client.Port
}

func parseTemplate(fileName string, data interface{}) (string, error) {
	t, err := template.ParseFiles(fileName)
	if err != nil {
		return "", err
	}

	buffer := new(bytes.Buffer)
	if err = t.Execute(buffer, data); err != nil {
		return "", err
	}

	log.Info("Template was parsed")

	return buffer.String(), nil
}

func getSubject() string {
	today := time.Now().Format(TimeFormat)
	weekAgo := time.Now().AddDate(0, 0, -7).Format(TimeFormat)

	return "New manga chapters (" + weekAgo + " - " + today + ")"
}
