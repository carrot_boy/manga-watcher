package pkg

import (
	"testing"
	"time"
)

const (
	AccessToken  = "2cd2d789-47d9-442d-908e-d197830d0b5b"
	RefreshToken = "0ed1c6b4-b847-464f-a317-b6ec1024fc92"
)

type TokenTestCase struct {
	token    Token
	expected bool
}

func TestIsExpired(t *testing.T) {
	for num, testCase := range getTestIsExpiredData() {
		if isExpired := testCase.token.IsExpired(); isExpired != testCase.expected {
			t.Fatalf("Token number %d should be %v, got %v\n", num, testCase.expected, isExpired)
		}
	}
}

func getTestIsExpiredData() []TokenTestCase {
	twoDaysAgo := time.Now().AddDate(0, 0, -2)
	expiredToken := createTokenWithDate(twoDaysAgo)

	twoHoursAgo := time.Now().Add(time.Hour * 2 * (-1))
	activeToken := createTokenWithDate(twoHoursAgo)

	return []TokenTestCase{
		{token: expiredToken, expected: true},
		{token: activeToken, expected: false},
	}
}

func createTokenWithDate(time time.Time) Token {
	return Token{
		AccessToken:  AccessToken,
		RefreshToken: RefreshToken,
		CreatedAt:    time.Unix(),
	}
}
