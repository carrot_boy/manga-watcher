package pkg

import (
	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/net/context"
	"log/syslog"
	"os"
	"regexp"
	"strings"
)

var (
	ClientId     = os.Getenv("CLIENT_ID")
	ClientSecret = os.Getenv("CLIENT_SECRET")

	MongoDbUri = os.Getenv("MONGODB_URI")

	SmtpHost     = os.Getenv("SMTP_HOST")
	SmtpPort     = os.Getenv("SMTP_PORT")
	SmtpUserName = os.Getenv("SMTP_USERNAME")
	SmtpPassword = os.Getenv("SMTP_PASSWORD")

	LogDestination = os.Getenv("LOG_DESTINATION")

	mongoContext context.Context
	mongoClient  *mongo.Client
)

const (
	LogNetwork  = "udp"
	LogTag      = "manga-watcher"
	LogPriority = syslog.LOG_INFO
)

func InitLogger() {
	writer := GetSyslogWriter()

	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(writer)
	log.SetLevel(log.InfoLevel)
}

func GetSyslogWriter() *syslog.Writer {
	syslogWriter, err := syslog.Dial(LogNetwork, LogDestination, LogPriority, LogTag)
	if err != nil {
		log.WithField("err", err).Fatal("failed to dial syslog")
	}

	return syslogWriter
}

func CreateRepository() RepositoryInterface {
	ctx := CreateMongoContext()
	mongoClient := createMongoClient(ctx)

	return Repository{
		context: ctx,
		client:  mongoClient,
	}
}

func CreateSourceApi(userId int64) ShikimoriInterface {
	return Shikimori{
		clientID:     ClientId,
		clientSecret: ClientSecret,
		userID:       userId,
	}
}

func createMongoClient(ctx context.Context) *mongo.Client {
	if mongoClient != nil {
		return mongoClient
	}

	clientOptions := options.Client().ApplyURI(MongoDbUri)
	mongoClient, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.WithError(err).Error("mongo.Connect() error")
		os.Exit(1)
	}
	log.Info("Mongo client created")

	return mongoClient
}

func CreateMongoContext() context.Context {
	if mongoContext != nil {
		return mongoContext
	}

	mongoContext, _ := context.WithTimeout(context.Background(), databaseTimeout)
	log.Info("Mongo context created")

	return mongoContext
}

func CreateSMTPClient() SMTPClientInterface {
	credentials := SMTPCredentials{
		Email:    SmtpUserName,
		Password: SmtpPassword,
	}

	return SMTPClient{
		Host:        SmtpHost,
		Port:        SmtpPort,
		Credentials: credentials,
	}
}

func GetCorrectTitle(urlPath string) string {
	re := regexp.MustCompile(`(__[^_]*)$`)
	cleanPath := re.ReplaceAll([]byte(strings.Trim(urlPath, "/")), []byte(""))

	return string(cleanPath)
}
