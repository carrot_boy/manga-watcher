package pkg

import (
	"time"
)

const (
	TimeToLive = time.Hour * 24
)

type Token struct {
	AccessToken  string `bson:"access_token" json:"access_token"`
	RefreshToken string `bson:"refresh_token" json:"refresh_token"`
	CreatedAt    int64  `bson:"created_at" json:"created_at"`
}

func (token Token) IsExpired() bool {
	expirationDuration := int64((TimeToLive).Seconds())
	expirationDate := token.CreatedAt + expirationDuration
	now := time.Now().Unix()

	return expirationDate <= now
}
