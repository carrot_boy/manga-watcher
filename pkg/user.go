package pkg

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	Database   = "manga-keeper"
	Collection = "users"
)

type User struct {
	Id       primitive.ObjectID `bson:"_id"`
	Email    string             `bson:"email"`
	ClientId int64              `bson:"client_id"`
	Weekday  string             `bson:"weekday"`
	Token    Token
}

func (user User) HasExpiredToken() bool {
	return user.Token.IsExpired()
}

func (user *User) UpdateToken() {
	api := CreateSourceApi(user.ClientId)
	newToken := api.RefreshToken(user.Token)

	repo := CreateRepository()
	isUpdated := repo.UpdateUserToken(user.Id, newToken)

	if isUpdated {
		user.Token = newToken
	}
}
